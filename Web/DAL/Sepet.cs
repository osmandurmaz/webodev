//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Web.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Sepet
    {
        public int Sepet_ID { get; set; }
        public int Kullanici_ID { get; set; }
        public int Urun_ID { get; set; }
        public Nullable<int> sepet_adet { get; set; }
        public Nullable<decimal> sepet_tutar { get; set; }
        public Nullable<System.DateTime> sepet_tarih { get; set; }
        public Nullable<bool> sepet_onay { get; set; }
    
        public virtual Kullanici Kullanici { get; set; }
        public virtual Urunler Urunler { get; set; }
    }
}
