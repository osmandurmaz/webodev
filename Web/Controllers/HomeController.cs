﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.DAL;
using Web.Models;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        OnlineMagazaEntities ome = new OnlineMagazaEntities();

        // GET: Urunler
        public ActionResult Index()
        {
            var urunler = ome.Urunler.Include(u => u.Kategori);
            return View(urunler.ToList());
        }
        

        public ActionResult Sepet()
        {

            if (Session["kid"] != null)
            {

                var kid = int.Parse(Session["kid"].ToString());

                List<Sepet> sepet = ome.Sepet.Where(x => x.Kullanici_ID == kid).ToList();
               
                return View(sepet);
                
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult SepeteEkle(Urunler sepetekle, decimal? BirimFiyat)
        {
            try
            {
                Urunler birim = new Urunler();
                Sepet _sepetekle = new Sepet();
                _sepetekle.Urun_ID = sepetekle.Urun_ID;
                _sepetekle.sepet_adet = 1;
                _sepetekle.Kullanici_ID = int.Parse(Session["kid"].ToString());
                _sepetekle.sepet_tarih = DateTime.Now;
                _sepetekle.sepet_tutar = BirimFiyat;
                Session["sepetToplam"] = Convert.ToDouble(Session["sepetToplam"]) + Convert.ToDouble(BirimFiyat);
                ome.Sepet.Add(_sepetekle);
                ome.SaveChanges();
                return RedirectToAction("Index");

            }
            catch (Exception ex)
            {
                throw new Exception("Sepete Eklemede Hata Oluştu", ex.InnerException);
            }

        }

        [HttpPost]
        public ActionResult Sepet(int KullaniciId)
        {
            try
            {
                List<Sepet> sepet = ome.Sepet.Where(x => x.Kullanici_ID == KullaniciId).ToList();

                foreach (var item in sepet)
                {
                    Siparis _urunekle = new Siparis();

                    _urunekle.Urun_ID = item.Urun_ID;
                    _urunekle.Sepet_ID = item.Sepet_ID;
                    _urunekle.siparis_adet = item.sepet_adet;
                    _urunekle.Kullanici_ID = KullaniciId;
                    _urunekle.siparis_tutar = item.sepet_tutar;
                    _urunekle.siparis_tarih = DateTime.Now;
                    _urunekle.siparis_sonuc = false;
                    ome.Siparis.Add(_urunekle);



                    ome.SaveChanges();

                    ome.Sepet.Remove(ome.Sepet.First(d => d.Kullanici_ID == KullaniciId));

                    ome.SaveChanges();

                    Session["sepetToplam"] = 0.0;
                }

                return RedirectToAction("Index");

            }
            catch (Exception ex)
            {
                throw new Exception("Siparis Eklemede Hata Oluştu", ex.InnerException);
            }
        }
        public ActionResult Urunler()
        {
            var urunler = ome.Urunler.Include(u => u.Kategori);
            return View(urunler.ToList());
        }

        public ActionResult Urun(int urunId)
        {
            var urun = ome.Urunler.Where(u => u.Urun_ID == urunId).FirstOrDefault();
            return View(urun);
        }

        public ActionResult Giris()
        {
            ViewBag.Message = "Your application description page.";

            Session["giris"] = null;
            return View();
        }

        public ActionResult Kaydol()
        {

            ViewBag.Message = "Your contact page.";

            return View();

        }

        [ValidateAntiForgeryToken]
        [HttpPost]

        public ActionResult Kaydol(Kullanici kaydolveriler)
        {
            try
            {
                Kullanici _kaydolveriler = new Kullanici();
                Sepet _sepet = new Sepet();
                _kaydolveriler.Ad = kaydolveriler.Ad;
                _kaydolveriler.Soyad = kaydolveriler.Soyad;
                _kaydolveriler.Dogum_Tarihi = kaydolveriler.Dogum_Tarihi;
                _kaydolveriler.Kullanici_adi = kaydolveriler.Kullanici_adi;
                _kaydolveriler.Kullanici_Sifre = kaydolveriler.Kullanici_Sifre;
                _kaydolveriler.Eposta = kaydolveriler.Eposta;
                _kaydolveriler.Telefon = kaydolveriler.Telefon;
                _kaydolveriler.Adres = kaydolveriler.Adres;
                ome.Kullanici.Add(kaydolveriler);
                ome.SaveChanges();
                ViewBag.Message = "Kullanici Kaydı Başarıyla Tamamlanmıştır";

                Session["kad"] = kaydolveriler.Kullanici_adi;
                Session["ad"] = kaydolveriler.Ad;
                Session["soyad"] = kaydolveriler.Soyad;
                Session["kid"] = kaydolveriler.Kullanici_ID;
                Session["sepetToplam"] = 0.00;

                Session["giris"] = true;

                TempData["mesaj"] = "Hoşgeldiniz";

                return RedirectToAction("Index", "Home");

            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu", ex.InnerException);
            }

        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GirisYardimci(GirisClass girisveriler)
        {

            var kullanici = ome.Kullanici.Where(x => x.Kullanici_adi == girisveriler.KullaniciAdi && x.Kullanici_Sifre == girisveriler.Sifre).FirstOrDefault();


            if (kullanici != null)
            {

                Session["kad"] = kullanici.Kullanici_adi;
                Session["ad"] = kullanici.Ad;
                Session["soyad"] = kullanici.Soyad;
                Session["kid"] = kullanici.Kullanici_ID;
                var sepet = ome.Sepet.Where(x => x.Kullanici_ID == kullanici.Kullanici_ID);
                foreach(var item in sepet.ToList())
                {
                    Session["sepetToplam"] = Convert.ToDouble(Session["sepetToplam"]) + Convert.ToDouble(item.sepet_tutar);
                }

                Session["giris"] = true;

                TempData["mesaj"] = "Hoşgeldiniz";

                return RedirectToAction("Index", "Home");


            }
            else
            {
                Session["giris"] = false;

            }
            ViewBag.Message = "Your contact page.";

            TempData["mesaj"] = "Giriş bilgileriniz yanlış";
            return RedirectToAction("Giris", "Home");

        }


        public class GirisClass
        {
            public string KullaniciAdi { get; set; }
            public string Sifre { get; set; }

        }
    }
}