﻿using Web.DAL;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Deneme.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        private OnlineMagazaEntities db = new OnlineMagazaEntities();
        
        #region // Urunler
        public ActionResult Urunler()
        {
            var urunler = db.Urunler.ToList();
            return View(urunler);
        }

        public ActionResult UrunEkle()
        {
            return View();
        }

        public ActionResult UrunDuzenle(int id)
        {
            var _urunDuzenle = db.Urunler.Where(x => x.Urun_ID == id).FirstOrDefault();
            return View(_urunDuzenle);
        }

        public ActionResult UrunSil(int id)
        {
            try
            {
                db.Urunler.Remove(db.Urunler.First(d => d.Urun_ID == id));
                db.SaveChanges();
                return RedirectToAction("Urunler", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }


        [HttpPost]
        public ActionResult UrunEkle(Urunler s, HttpPostedFileBase file)
        {
            try
            {
                Urunler _urun = new Urunler();
                if (file != null && file.ContentLength > 0)
                {
                    string[] test = file.FileName.Split('\\');
                    string name = test.LastOrDefault();

                    _urun.Urun_resim = "images/Urunler/" + name;
                }


                _urun.Urun_adi = s.Urun_adi;


                _urun.Urun_detay = s.Urun_detay;
                _urun.Fiyat = s.Fiyat;

                _urun.Kategori_ID = s.Kategori_ID;
                _urun.Barcode_Num = s.Barcode_Num;


                var request = (HttpWebRequest)WebRequest.Create("http://46.101.210.195/mert/v1/createcampaign");

                var postData = "Urun_resim=m1";
                postData += "&Urun_adi=deneme";
                postData += "&Urun_detay=deneme1";
                postData += "&Fiyat=100";
                postData += "&Kategori_ad=1";
                postData += "&Barcode_Num=100";

                var data = Encoding.ASCII.GetBytes(postData);

                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();








                //            using (WebClient client = new WebClient())
                //            {

                //                byte[] response =
                //                client.UploadValues("http://46.101.210.195/mert/v1/createcampaign", new NameValueCollection()
                //{
                //    { "Urun_adi", "aaaa" },
                //    { "Urun_detay", "bbbb" }
                //});

                //                string result = System.Text.Encoding.UTF8.GetString(response);
                //            }



                //            using (WebClient client = new WebClient())
                //            {

                //                byte[] response =
                //                client.UploadValues("http://46.101.210.195/mert/v1/createcampaign", new NameValueCollection()
                //         {   
                //  //  { "Urun_resim", s.Urun_resim },
                //  //  { "Urun_adi", s.Urun_adi }
                //   // { "Urun_detay", s.Urun_detay},
                //   // { "Fiyat" , s.Fiyat.ToString()},
                //   // {"Kategori_ad", s.Kategori_ID.ToString()},
                //  //  {"Barcode_Num",s.Barcode_Num.ToString()}
                //});

                //                string result = System.Text.Encoding.UTF8.GetString(response);
                //            }

                //var gizmo = new Urunler() { Urun_adi = "Gizmo", Fiyat = 100, Urun_detay = "Widget" };

                //var response = await client.PostAsJsonAsync("https://posttestserver.com/post.php", gizmo);
                //if (response.IsSuccessStatusCode)
                //{
                //     Get the URI of the created resource.
                //    Uri gizmoUrl = response.Headers.Location;
                //}

                db.Urunler.Add(_urun);
                db.SaveChanges();

                return RedirectToAction("Urunler", "Admin");


                //HttpClient client = new HttpClient();
                //// HTTP POST
                //var gizmo = new Urunler() { Urun_adi = "Gizmo", Fiyat = 100, Urun_detay = "Widget" };
                //HttpResponseMessage response = new HttpResponseMessage();
                //response =  client.PostAsJsonAsync("questions/31360388/postasjsonasync-cannot-find-requested-uri", gizmo);

                //if (response.IsSuccessStatusCode)
                //{
                //    // Get the URI of the created resource.
                //    Uri gizmoUrl = response.Headers.Location;
                //}
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu", ex.InnerException);
            }
        }

        //[HttpPost]
        //public ActionResult deneme()
        //{

        //    HttpClient client = new HttpClient();
        //    // HTTP POST
        //    var gizmo = new Urunler() { Urun_adi = "Gizmo", Fiyat = 100, Urun_detay = "Widget" };
        //    HttpResponseMessage response = new HttpResponseMessage();
        //    response = await client.PostAsJson("https://posttestserver.com/post.php", gizmo);

        //    if (response.IsSuccessStatusCode)
        //    {
        //        // Get the URI of the created resource.
        //        Uri gizmoUrl = response.Headers.Location;
        //    }
        //}
        [HttpPost]
        public ActionResult UrunDuzenle(Urunler urun, HttpPostedFileBase file)
        {
            try
            {
                var _urunDuzenle = db.Urunler.Where(x => x.Urun_ID == urun.Urun_ID).FirstOrDefault();
                if (file != null && file.ContentLength > 0)
                {

                    _urunDuzenle.Urun_resim = "images/sebzeler/" + file.FileName;
                }
                _urunDuzenle.Urun_adi = urun.Urun_adi;


                _urunDuzenle.Urun_detay = urun.Urun_detay;
                _urunDuzenle.Fiyat = urun.Fiyat;

                _urunDuzenle.Kategori_ID = urun.Kategori_ID;
                _urunDuzenle.Barcode_Num = urun.Barcode_Num;
                db.SaveChanges();
                return RedirectToAction("Urunler", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }

        #endregion

        #region // Kullanici

        public ActionResult Kullanici()
        {
            var kullanicilar = db.Kullanici.ToList();
            return View(kullanicilar);
        }
        public ActionResult KullaniciEkle()
        {
            return View();
        }
        public ActionResult KullaniciDuzenle(int id)
        {
            var _kullaniciduzenle = db.Kullanici.Where(x => x.Kullanici_ID == id).FirstOrDefault();
            return View(_kullaniciduzenle);
        }
        public ActionResult KullaniciSil(int id)
        {
            try
            {
                db.Kullanici.Remove(db.Kullanici.First(d => d.Kullanici_ID == id));
                db.SaveChanges();
                return RedirectToAction("Kullanici", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }


        [HttpPost]
        public ActionResult KullaniciEkle(Kullanici kll)
        {
            try
            {
                Kullanici _kll = new Kullanici();
                _kll.Ad = kll.Ad;
                _kll.Soyad = kll.Soyad;
                _kll.Telefon = kll.Telefon;
                _kll.Eposta = kll.Eposta;
                _kll.Dogum_Tarihi = kll.Dogum_Tarihi;
                _kll.Kullanici_adi = kll.Kullanici_adi;
                _kll.Kullanici_Sifre = kll.Kullanici_Sifre;
                _kll.Telefon = kll.Telefon;
                _kll.Adres = kll.Adres;
                _kll.il_ad = kll.il_ad;
                db.Kullanici.Add(_kll);
                db.SaveChanges();
                return RedirectToAction("Kullanici", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu", ex.InnerException);
            }
        }
        [HttpPost]
        public ActionResult KullaniciDuzenle(Kullanici kullanici)
        {
            try
            {
                var _kullaniciDuzenle = db.Kullanici.Where(x => x.Kullanici_ID == kullanici.Kullanici_ID).FirstOrDefault();
                _kullaniciDuzenle.Ad = kullanici.Ad;
                _kullaniciDuzenle.Soyad = kullanici.Soyad;
                _kullaniciDuzenle.Telefon = kullanici.Telefon;
                _kullaniciDuzenle.Eposta = kullanici.Eposta;
                _kullaniciDuzenle.Dogum_Tarihi = kullanici.Dogum_Tarihi;
                _kullaniciDuzenle.Kullanici_adi = kullanici.Kullanici_adi;
                _kullaniciDuzenle.Kullanici_Sifre = kullanici.Kullanici_Sifre;
                _kullaniciDuzenle.Telefon = kullanici.Telefon;
                _kullaniciDuzenle.Adres = kullanici.Adres;
                _kullaniciDuzenle.il_ad = kullanici.il_ad;
                db.SaveChanges();
                return RedirectToAction("Kullanici", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }

        #endregion

    }
}
